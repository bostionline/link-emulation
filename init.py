#!/usr/bin/env python

from neutronclient.v2_0 import client
from credentials import get_credentials
from novaclient import client as novaclient
from credentials import get_nova_creds
from time import sleep
from Rules import deleteRules

creds = get_nova_creds()
nova = novaclient.Client("2", **creds)
credentials = get_credentials()
neutron = client.Client(**credentials)

instance= nova.servers.list()
network_list=nova.networks.list()
routers_list = neutron.list_routers()
router=routers_list['routers']

#print neutron.list_ports()
ports=neutron.list_ports()
port=ports['ports']


# delete old instances
print "Look for instance."
for i in range(len(instance)):
   try:
      instance[i].delete()
   finally:
         print ("delete "+str(instance[i]))

# wait for instances
waiten=nova.servers.list()

while waiten:
      waiten=nova.servers.list()
      print ("waiting until list down:"+str(waiten))
      sleep(5)

print "There are no instances now."


for i in range(len(router)):
 if (str(router[i][u'name'])=='router1'):print ("found "+str(router[i][u'name'])+" OK") 
 else: 
	try:
         neutron.delete_router(router[i][u'id'])
        finally:
           print ("delete" + str(router[i][u'name']))

print "Routers deleted."

#delete ports

for i in range(len(port)):
  if (str(port[i][u'name'])==''):print ("found "+"free Port "+" OK")
  else: 
	try:
         neutron.delete_port(port[i][u'id'])
        finally:
           print ("delete " + str(port[i][u'name']))

print "Ports deleted."
  





print "Look for networks."
#delete networks
for i in range(len(network_list)):
 if (str(network_list[i])=='<Network: public>' or str(network_list[i])=='<Network: private>'):print ("found"+str(network_list[i])+" OK") 
 else: 
	try:
         neutron.delete_network(network_list[i].id)
        finally:
           print ("delete" + str(network_list[i]))

print "There are only privat and public networks."



#neutron.DeleteRouter

deleteRules()

