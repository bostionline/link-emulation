

def createRules():
 from novaclient import client as novaclient
 from credentials import get_nova_creds
 creds = get_nova_creds()
 nova = novaclient.Client("2", **creds)

#print nova.security_groups.list()
 secgroup = nova.security_groups.find(name="default")
#print secgroup.id

 nova.security_group_rules.create(secgroup.id, ip_protocol="tcp", from_port="22", to_port="22", cidr="0.0.0.0/0")
 nova.security_group_rules.create(secgroup.id, ip_protocol="icmp", from_port=-1, cidr="0.0.0.0/0", to_port=-1)
 print "Start ssh and icmp Rules."

def deleteRules():
 from novaclient import client as novaclient
 from credentials import get_nova_creds
 creds = get_nova_creds()
 nova = novaclient.Client("2", **creds)
 secgroup = nova.security_groups.find(name="default")
 rule=secgroup.rules

 for i in range(len(rule)):
  if (str(secgroup.rules[i][u'from_port'])=="22"):
   nova.security_group_rules.delete(secgroup.rules[i][u'id'])
  if (str(secgroup.rules[i][u'from_port'])=="-1"):
   nova.security_group_rules.delete(secgroup.rules[i][u'id'])
 print "Delete ssh and icmp Rules."

