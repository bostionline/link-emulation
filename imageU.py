#!/usr/bin/env python
import keystoneclient.v2_0.client as ksclient
import glanceclient
from credentials import get_credentials
creds = get_credentials()
keystone = ksclient.Client(**creds)
glance_endpoint = keystone.service_catalog.url_for(service_type='image',
                                                   endpoint_type='publicURL')
glance = glanceclient.Client('1',glance_endpoint, token=keystone.auth_token)
with open('/home/basti/devstack/my_Images/Ubuntueth4.img') as fimage:
# with open('/home/basti/devstack/my_Images/ubuntu-12.04-server-cloudimg-amd64-disk1.img') as fimage:
    glance.images.create(name="Ubuntueth4", is_public=True, disk_format="qcow2",
                         container_format="bare", data=fimage)
