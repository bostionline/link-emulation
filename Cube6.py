#!/usr/bin/env python


import os
from neutronclient.v2_0 import client
from credentials import get_credentials
from novaclient import client as novaclient
from credentials import get_nova_creds
from time import sleep
from Rules import createRules
from Rules import deleteRules

createRules()
creds = get_nova_creds()
nova = novaclient.Client("2", **creds)
n=[]
credentials = get_credentials()
neutron = client.Client(**credentials)

a=nova.networks.list()

if str(a[0])=='<Network: private>':
  main_Net=a[0].id
else: main_Net=a[1].id

#Start Network

# network_name = ['Edge12','Edge23','Edge34','Edge41','Edge112','Edge123','Edge134','Edge141','wless1','wless2','wless3','wless4']
# ip_addr=['10.0.3.0/24','10.0.4.0/24','10.0.5.0/24','10.0.6.0/24','10.0.7.0/24','10.0.8.0/24','10.0.9.0/24','10.0.10.0/24','10.0.11.0/24','10.0.12.0/24','10.0.13.0/24','10.0.14.0/24']

network_name=[]
ip_addr=[]
max_layer = 4
max_node_line = 1
ip = 0
info = {'Edge 000 000': '0000'}

for layer in range(1,max_layer+1):
 for i in range(1,max_node_line+1):
  for j in range(1,max_node_line):
       network_name.append("Edge " + str(layer) + str(i) + str(j) + " " + str(layer) + str(i) + str(j+1))	
       ip = ip + 1       
       ip_addr.append("10." + str(layer) + "." + str(ip) + "." + "0/24")

      

for layer in range(1,max_layer+1):
 for i in range(1,max_node_line):
  for j in range(1,max_node_line+1):
       network_name.append("Edge " + str(layer) + str(i) + str(j) + " " + str(layer) + str(i+1) + str(j))	
       ip = ip + 1       
       ip_addr.append("10." + str(layer) + "." + str(ip) + "." + "0/24")

     


for layer in range(1,max_layer):
 for i in range(1,max_node_line+1):
  for j in range(1,max_node_line+1):
       network_name.append("Edge " + str(layer) + str(i) + str(j) + " " + str(layer+1) + str(i) + str(j))	
       ip = ip + 1       
       ip_addr.append("10." + str(layer) + "." + str(ip) + "." + "0/24")

#print network_name, ip_addr

for i in range(len(network_name)):

   try:
       body_sample = {'network': {'name': network_name[i],
                      'admin_state_up': True}}
  
       netw = neutron.create_network(body=body_sample)
       net_dict = netw['network']
       network_id = net_dict['id']
       info[network_name[i]]=network_id
       n.append(network_id)
#      print('Network %s created' % network_id)

       body_create_subnet = {'subnets': [{'cidr': ip_addr[i],
                            'ip_version': 4, 'network_id': n[i]}]}

       subnet = neutron.create_subnet(body=body_create_subnet)
#      print('Created subnet %s' % subnet)
   finally:
       print("Create Network: " + str(network_name[i]))

#       print info

# Start the instances
instance=[]
iname=[]
#image = nova.images.find(name="cirros-0.3.4-x86_64-uec")
#image = nova.images.find(name="Cirros_3eth")
#flavor = nova.flavors.find(name="cirros256")
image = nova.images.find(name="Ubuntueth4")
flavor = nova.flavors.find(name="ds512M")


if not nova.keypairs.findall(name="mykey"):
   with open(os.path.expanduser('~/.ssh/id_rsa.pub')) as fpubkey:
       nova.keypairs.create(name="mykey", public_key=fpubkey.read())


for layer in range(1,max_layer+1):
 for i in range(1,max_node_line+1):
  for j in range(1,max_node_line+1):
     name="Node "+str(layer)+str(i)+str(j)
     iname.append(name)
     nics=[]
     nics.append({"net-id": main_Net, "v4-fixed-ip": ''})
     # x-1,y,z  info['edge x-1 y z x y z]
     if layer>1:
                nics.append({"net-id": info['Edge '+str(layer-1)+str(i)+str(j)+' '+str(layer)+str(i)+str(j)], "v4-fixed-ip": ''})
     if layer<max_layer:
                nics.append({"net-id": info['Edge '+str(layer)+str(i)+str(j)+' '+str(layer+1)+str(i)+str(j)], "v4-fixed-ip": ''})

     if i>1:
                nics.append({"net-id": info['Edge '+str(layer)+str(i-1)+str(j)+' '+str(layer)+str(i)+str(j)], "v4-fixed-ip": ''})
     if i<max_node_line:
                nics.append({"net-id": info['Edge '+str(layer)+str(i)+str(j)+' '+str(layer)+str(i+1)+str(j)], "v4-fixed-ip": ''})

     if j>1:
                nics.append({"net-id": info['Edge '+str(layer)+str(i)+str(j-1)+' '+str(layer)+str(i)+str(j)], "v4-fixed-ip": ''})
     if j<max_node_line:
                nics.append({"net-id": info['Edge '+str(layer)+str(i)+str(j)+' '+str(layer)+str(i)+str(j+1)], "v4-fixed-ip": ''})


     try:

	 instance.append(nova.servers.create(name=name, image=image, flavor=flavor, nics=nics,key_name="mykey"))
        
     finally:
       	  print("start "+ name)

print iname
#print instance
raw_input("press any key to delete!")

#delete instances
for i in range(len(instance)):
   try:
      instance[i].delete()
   finally:
         print ("delete "+str(instance[i]))

# wait for instances
waiten=nova.servers.list()

while waiten:
      waiten=nova.servers.list()
      print ("waiting until list down:"+str(waiten))
      sleep(5)
      
#delete networks
for i in range(len(network_name)):
   try:
      neutron.delete_network(n[i])
   finally:
          print ("delete" + str(network_name[i]))

deleteRules()





























